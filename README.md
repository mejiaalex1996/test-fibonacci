# Test Fibonacci

**PLANTEAMIENTO DEL PROBLEMA**
 
Se necesita una función que reciba como parámetro un numero entero N y que retorne como resultado la suma del valor de la serie fibonacci en la posición N más el valor de la serie fibonacci en  la posición (N + 1)
 
**Formato de entrada**

    N (parámetro de entrada) es un numero entero donde N > 0 y N < 12
    S (valor de retorno) es el resultado de la suma de Fibonnaci(n) + Fibonnaci(n+1)
 
**Ejemplos**

    n => fibonnaci(n) + fibonnaci(n+1) = resultado
    entrada: 1 => 1 + 1 => 2
    entrada: 2 => 1 + 2 => 3
    entrada: 3 => 2 + 3 => 7
    entrada: 11 => 89 + 144  = 233
 
 
**Entregables:**

* Pseudocódigo.
* Función programada en el lenguaje favorito.

**Puntos adicionales**

* Código alojado en algún servicio de control de versiones como Github, Gitlab, etc.
* Función recursiva.
* 

**RESOLUCIÓN CON PSEUDOCODIGO**

        INTEGER n = DATO DE FORMULARIO [NUMBER]; //RECIBE DATO DE FORMULARIO

        INTEGER number = 0; //DECLARAS VARIABLE number COMO 0
    
        FOR(INTEGER i = n; i <= n+1; i++) //RECORRES LA VARIABLE RECIBIDA
            number += fibonacci(i); //MANDAS A LLAMAR LA FUNCION fibonacci Y SUMAS EL RESULTADO
        IMPRIME number; //REGRESAS EL VALOR YA PREVIAMENTE SUMADO
    
        function fibonacci(Recibe parametro x)
            IF(x<=0) //SI ES CERO O MENOR REGRESAS UN 0
                REGRESA 0;
            ELSEIF(x==1) //SI ES UNO REGRESAS UN 1
                REGRESA 1;
            REGRESA fibonacci(x-1)+fibonacci(x-2); CREAS LA RECURSIVIDAD EN UN VALOR NO MENOR A CERO Y NO IGUAL A UNO, PARA PODER SEGUIR LA SERIE DE FIBONACCI.
            

**ESTRUCTURA DEL CÓDIGO**

* Existen dos archivos uno llamado form.php y otro fibonacci.php.
* El archivo de form funciona para poder indicar que número voy a asignar a la serie de fibonacci.
* El archivo de fibonacci funciona para ejecutar todo el proceso de la serie.
* Para correrlos use XAMP el cual lo monte en mi computadora personal y los ejecute en localhost en mi navegador.



        